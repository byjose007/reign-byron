import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  baseUrl =  environment.apiUrl;

  constructor( private http: HttpClient) {
  }

  getAll(){
    return this.http.get<any[]>(this.baseUrl + '/list');
  }

  get(id:string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  delete(id:string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl);
  }

  findByTitle(title:string): Observable<any> {
    return this.http.get(`${this.baseUrl}?title=${title}`);
  }
}
