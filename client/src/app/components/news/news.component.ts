import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  public news: any;
  faTrashAlt = faTrashAlt


  constructor(private newsService: NewsService) { }

  ngOnInit(): void {

    this.loadNews();

  }

  //   author: "pwdisswordfish6"
  // comment_text: "True. But it would have been much harder if they had to define a syntax for the entire document.<p>I imagine they thought that the path DSL is simple enough to parse (and I seem to vaguely recall PostScript has something similar), while the overhead of representing path nodes as XML elements would be too high."
  // created_at: "2021-02-12T18:47:28.000Z"
  // created_at_i: "1970-01-19T16:05:55.648Z"
  // num_comments: null
  // objectID: "26117005"
  // parent_id: 26116919
  // points: null
  // story_id: 26114863
  // story_text: null
  // story_title: "SVG: The Good, the Bad and the Ugly"
  // story_url: "https://www.eisfunke.com/article/svg-the-good-the-bad-and-the-ugly.html"
  // title: null
  // url: null
  // __v: 0
  // _id: "6026ce1fdf53cd6afbb92244"


  loadNews() {
    this.newsService.getAll().subscribe((news) => {
      console.log(news, 'noticias.......');
      this.news = news;
    });
  }


  delete(id:string) {
 
    this.newsService.delete(id).subscribe(post =>{
      console.log('eliminado', post);
      
    })

  }

  detail(url:string){
    window.open(url, "_blank");
    
  }

}
