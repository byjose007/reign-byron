import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './news/news.module';
import { GetNewsService } from './news/jobs/get-news/get-news.service';
import { NewsService } from './news/news.service';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/reign', {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
  }),
  NewsModule,
  HttpModule,
  ScheduleModule.forRoot()], 
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
