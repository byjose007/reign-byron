import { HttpService, Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";


import { News } from "./interfaces/news.interface";
import { newsDTO } from "./dto/news.dto";
import { Cron } from '@nestjs/schedule';

@Injectable()
export class NewsService {

    constructor(
        private httpService: HttpService,
        @InjectModel('News') private readonly newsModel: Model<News>,
       ) { }

    // Get all News
    async getAllNews(): Promise<News[]> {
        const News = await this.newsModel.find();
        return News;
    }

    // Get a single News
    async getNews(id: string): Promise<News> {
        const news = await this.newsModel.findById(id);
        return news;
    }

    // Post news
    async createNews(newsDTO: newsDTO): Promise<News> {

        const News = new this.newsModel(newsDTO);
        return News.save();
    }



    // Delete News
    async deleteNews(id: string): Promise<any> {
        const deletedNews = await this.newsModel.findByIdAndDelete(id);
        return deletedNews;
    }

    // Put a single news
    async updateNews(id: string, newsDTO: newsDTO): Promise<News> {
        const updatedNews = await this.newsModel
            .findByIdAndUpdate(id, newsDTO, { new: true });
        return updatedNews;
    }


    async getDataApi(){
        return new Promise((resolve, reject) => {
            this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
                .subscribe((data: any) => {
                    console.log(data.data, 'data......');
                    resolve(data.data);
                });
        });
      }


 
}
