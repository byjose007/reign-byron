
import {
    Controller, Get, Post, Delete, Put, Body, Param, Res, Response,
    HttpStatus, UseInterceptors, NotFoundException} from '@nestjs/common';
import { newsDTO } from './dto/news.dto';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {

    constructor(private newsService: NewsService) { }

    @Post('/')
    async createNews(@Res() res, @Body() newsDTO: newsDTO) {
        const customer = await this.newsService.createNews(newsDTO);
        return res.status(HttpStatus.OK).json({
            message: 'News Successfully Created',
            customer,
        });
    }


    @Get('/list')
    async getAllNews(@Res() res) {
        const news = await this.newsService.getAllNews();
        return  res.status(HttpStatus.OK).json(news);
    }

    @Get('/:id')
    async getNews(@Res() res, @Param('id') id:string) {
        const customer = await this.newsService.getNews(id);
        if (!customer) { throw new NotFoundException('News does not exist!'); }
        return res.status(HttpStatus.OK).json(customer);
    }


    @Delete('/:id')
    async deleteNews(@Res() res, @Param('id') id:string) {
        const customerDeleted = await this.newsService.deleteNews(id);
        if (!customerDeleted) { throw new NotFoundException('News does not exist!'); }
        return res.status(HttpStatus.OK).json({
            message: 'News Deleted Successfully',
            customerDeleted,
        });
    }


    @Put('/')
    async updateNews(@Res() res, @Body() newsDTO: newsDTO, @Param('id') id) {
        const updatedNews = await this.newsService.updateNews(id, newsDTO);
        if (!updatedNews) { throw new NotFoundException('News does not exist!'); }
        return res.status(HttpStatus.OK).json({
            message: 'News Updated Successfully',
            updatedNews,
        });
    }

}

