import { HttpModule, Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';

// Mongoose
import { MongooseModule } from '@nestjs/mongoose';
import { NewsSchema } from './schemas/news.schema';
import { ScheduleModule } from '@nestjs/schedule';
import { GetNewsService } from './jobs/get-news/get-news.service';


@Module({

  imports: [
    MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }]),
    HttpModule
  ],

  providers: [NewsService, GetNewsService],
  controllers: [NewsController],
  exports: [ NewsService, GetNewsService]
})
export class NewsModule {}
