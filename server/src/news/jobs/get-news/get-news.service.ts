import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { News } from 'src/news/interfaces/news.interface';
import { NewsService } from 'src/news/news.service';

@Injectable()
export class GetNewsService {

    constructor(
        @InjectModel('News') private readonly newsModel: Model<News>,
        private newsService: NewsService) { }

    // @Cron('10 * * * * *')
    @Cron('* 0 * * * *') //every hour
    async handleCron() {
        let hackersNews: any = await this.newsService.getDataApi();
        
        for (let post of hackersNews.hits) {
            delete post._highlightResult;
            const postExists = await this.newsModel.findOne({ objectID: post.objectID });
            // ------------ if it doesn't exist, create ---------
            if (!postExists) {
                const News = new this.newsModel(post);
                await News.save();
            }
        }







    }
}

